const User = require("../models/user.js")
const Course = require("../models/course.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")

module.exports.checkEmail = (requestBody) => {
	return User.find({ email: requestBody.email}).then((result, error) =>{
		if (error) {
			console.log(error)
			return false
		}else{ 
			if (result.length > 0) {
				return true } else {
					return false
				}
	}
})
}

module.exports.registerUser = (reqBody) =>{
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        age: reqBody.age,
        gender: reqBody.gender,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
        mobileNo: reqBody.mobileNo
    })
    return newUser.save().then((saved, error) => {
        if(error){
            console.log(error)
            return false
        }else{
            return true
        }
    })
}

// USER Login
/* 
1. find if the email is existing in the database
2. check if pw is correct
*/

module.exports.userLogin = (reqBody) => {
    return User.findOne({email:reqBody.email}).then(result => {
        if (result === null){
            return false
        }else{
            // bcrypt.compareSync function - used to compare a non-encrypted password to an encrypted password and returns a Boolean response depending on the result
            /* 
            What should we do after the comparison?
                true - a token should be created since the user is existing and the password is correct
                false - the passwords do not match, thus a token should not be created
            */
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
            if(isPasswordCorrect){
                return {access: auth.createAccessToken(result.toObject())}
            }else{
                return false
            }
        }
    })
}

module.exports.getProfile = (data) => {
    return User.findById(data.userId).then(result => {
        if (result === null){
            return false
        }else{
            result.password = ""
            return result
        }
    })
}

